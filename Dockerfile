ARG TAG=
FROM debian:${TAG}

# Args
ARG TAG
ARG OVERLAY_VERSION="3.2.0.2"

# Labels
LABEL VERSION="Debian ${TAG}"

# Environment variables
ENV \
  PS1="$(whoami)@$(hostname):$(pwd)\\$ " \
  HOME="/root" \
  TERM="xterm"

RUN \
  echo "**** Building Debian ${TAG} ****" && \
  echo "**** Install build packages ****" && \
  apt update && \
  apt full-upgrade -y && \
  echo "**** Install runtime packages ****" && \
  apt install -y --no-install-recommends \
    bash \
    ca-certificates \
    coreutils \
    curl \
    tar \
    xz-utils \
    tzdata && \
  echo "**** Add s6 overlay ****" && \
  # Find arch for archive
  ARCH=$(uname -m) && \
  OVERLAY_ARCH="" && \
  [ "${ARCH}" = "x86_64" ] && OVERLAY_ARCH="x86_64" || true && \
  [ "${ARCH}" = "aarch64" ] && OVERLAY_ARCH="aarch64" || true && \
  [ "${ARCH}" = "armv7l" ] && OVERLAY_ARCH="armhf" || true && \
  # S6 scripts
  curl -fsS -O --output-dir /tmp/ -L \
    "https://github.com/just-containers/s6-overlay/releases/download/v${OVERLAY_VERSION}/s6-overlay-noarch.tar.xz" && \
  tar -C / -Jxpf "/tmp/s6-overlay-noarch.tar.xz" && \
  # S6 binary
  curl -fsS -O --output-dir /tmp/ -L \
    "https://github.com/just-containers/s6-overlay/releases/download/v${OVERLAY_VERSION}/s6-overlay-${OVERLAY_ARCH}.tar.xz" && \
  tar -C / -Jxpf "/tmp/s6-overlay-${OVERLAY_ARCH}.tar.xz" && \
  # S6 symlinks
  curl -fsS -O --output-dir /tmp/ -L \
    "https://github.com/just-containers/s6-overlay/releases/download/v${OVERLAY_VERSION}/s6-overlay-symlinks-noarch.tar.xz" && \
  tar -C / -Jxpf "/tmp/s6-overlay-symlinks-noarch.tar.xz" && \
  curl -fsS -O --output-dir /tmp/ -L \
    "https://github.com/just-containers/s6-overlay/releases/download/v${OVERLAY_VERSION}/s6-overlay-symlinks-arch.tar.xz" && \
  tar -C / -Jxpf "/tmp/s6-overlay-symlinks-arch.tar.xz" && \
  echo "**** Create abc user and make our folders ****" && \
  groupmod -g 1000 users && \
  groupadd -g 911 abc && \
  useradd -u 911 -g abc -G users -d /config -s /bin/false abc && \
  mkdir -p \
    /app \
    /config \
    /defaults && \
  echo "**** Cleanup ****" && \
  rm -rf \
    /usr/share/doc/* \
    /var/lib/apt/lists/* \
    /var/cache/apt/archives/*.deb \
    /tmp/*

# Add extra files
COPY root/ /

ENTRYPOINT ["/init"]
